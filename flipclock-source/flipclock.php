<?php

namespace vt\flipclock_countdown;

add_shortcode('flipclock_countdown', __NAMESPACE__ . '\shortcode_flipclock');

add_action('wp_enqueue_scripts', __NAMESPACE__ . '\flipclock_styles');
add_action('wp_enqueue_scripts', __NAMESPACE__ . '\flipclock_scripts');

function shortcode_flipclock($atts) {

	$atts = shortcode_atts(array(
		'year' => '2021',
		'month' => '1',
		'day' => '1',
		'hour' => '0',
		'minute' => '0',
	), $atts);
	wp_enqueue_style('flipclock');
	wp_enqueue_script('flipclock');
	ob_start();
	?>
	<div>

		<div class="clock" style="margin:2em;"></div>

		<script type="text/javascript">
			var clock;

			jQuery(document).ready(function($) {

				// Grab the current date
				var currentDate = new Date();

				// Set some date in the future. In this case, it's always Jan 1
				var futureDate  = new Date(<?php echo $atts['year'] . ', ' . $atts['month'] . ', ' . $atts['day'] . ', ' . $atts['hour'] . ', ' . $atts['minute'] ?> , 0, 0);

				// Calculate the difference in seconds between the future and current date
				var diff = futureDate.getTime() / 1000 - currentDate.getTime() / 1000;

				// Instantiate a coutdown FlipClock
				clock = $('.clock').FlipClock(diff, {
					clockFace: 'DailyCounter',
					countdown: true
				});
			});
		</script>
	</div>
<?php
return ob_get_clean();

}

function flipclock_styles() {
	wp_register_style(
		'flipclock',
		SHORTCODE_DEMO_URL . 'flipclock-source/flipclock.css',
		array()
	);
}

function flipclock_scripts() {
	wp_register_script(
		'flipclock', SHORTCODE_DEMO_URL . 'flipclock-source/flipclock.js',
		array('jquery', 'jquery-ui-core'), '1.0', 'all'
	);

}
