<?php
/*

 * Plugin Name: Flipclock Countdown WP plugin
 * Plugin URI: https://gitlab.com/vaniatejuco/flipclock-countdown__wp-plugin
 * Description: ported flipclock.js to wordpress as a plugin
 * Version: 1.0.0
 * Author: Vania Tejuco
 * Author URI: https://www.vaniatejuco.com
 * License: GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt

 */

namespace vt\flipclock_countdown;

// stop unwanted visitors calling directly

if (!defined('ABSPATH')) {
	exit('Go Away!!');
}

$myplugin_url = plugin_dir_url(__FILE__);
if (is_ssl()) {
	$myplugin_url = str_replace('http://', 'https://', $myplugin_url);
}

define('SHORTCODE_DEMO_URL', $myplugin_url);
define('SHORTCODE_DEMO__DIR', plugin_dir_path(__FILE__));

include "flipclock-source/flipclock.php";